~ function () {
	'use strict';
	var $ = TweenMax,
		spriteSheet,
		ad = document.getElementById('mainContent'),
		bgExit = document.getElementById('bgExit'),	
		easing = Power3.easeInOut;

	window.init = function () {
		play();
		addListeners();
		
		spriteSheet = new spriteSheetAnim('mainAnimation',
				'img/spriteSheet.png',
				200, 200, 2000,
				1000, 48, false, 100);
		
	}

	function play() {

		var tl = new TimelineLite();
		tl.set('#mainContent', { force3D: true })
		tl.addLabel('frame')
		tl.add(function(){spriteSheet.ss_playRange(48,0)});
		tl.to('#mainAnimation',0.5, {opacity:0, ease:Power1.easeInOut},'frame+=5');
		tl.to(['#logo','#endCopy','.bgCircle'], 0.5,{ opacity: 1, ease:Power1.easeInOut }, 'frame+=5.1');

	}
	
	function addListeners() {
		bgExit.addEventListener('click', bgExitHandler);
	}

	function bgExitHandler(e) {
		Enabler.exit("Background Exit");
	}

}();

