var spriteSheetAnim = /** @class */ (function () {
    function spriteSheetAnim(animDiv, imgUrl, frameWidth, frameHeight, sheetWidth, sheetHeight, totalFrames, allowLoop, speed) {
        var _this = this;
        this.global = window;
        this.currState = "s_pause";
        this.allowLoop = false;
        // DO NOT EDIT VARS
        this.currFrame = 1;
        this.rangeStart = 0;
        this.rangeEnd = 1;
        this.mainLoop = null;
        this.setUpSheet = function () {
            _this.animDiv.style.position = "absolute";
            _this.animDiv.style.width = _this.frameWidth + "px";
            _this.animDiv.style.height = _this.frameHeight + "px";
            _this.animDiv.style.backgroundImage = "url(" + _this.imgUrl + ")";
            _this.animDiv.style.backgroundRepeat = "no-repeat";
            _this.animDiv.style.backgroundPosition = "0px 0px";
        };
        this.requestFrame = function () {
            if (_this.currState == "s_play") {
                if (_this.currFrame > _this.totalFrames) {
                    _this.currFrame = 1;
                }
                else if (_this.currFrame == _this.totalFrames && _this.allowLoop == false) {
                    _this.currState = "s_pause";
                    _this.currFrame = _this.totalFrames;
                }
                var xpos = ((_this.currFrame - 1) % _this.spritesInX);
                var ypos = Math.floor(((_this.currFrame - 1) / _this.spritesInX));
                _this.animDiv.style.backgroundPosition = (-xpos * _this.frameWidth) + "px " + (-ypos * _this.frameHeight) + "px";
                _this.currFrame++;
            }
            else if (_this.currState == "s_rewind") {
                if (_this.currFrame < 1) {
                    _this.currFrame = _this.totalFrames;
                }
                else if (_this.currFrame < 1 && _this.allowLoop == false) {
                    _this.currFrame = 1;
                    _this.currState = "s_pause";
                }
                if (_this.currFrame < 1) {
                    _this.currFrame = _this.totalFrames;
                }
                _this.upDateSheet();
                _this.currFrame--;
            }
            else if (_this.currState == "play_range") {
                if (_this.rangeStart < _this.rangeEnd) {
                    // Playing forward
                    if (_this.currFrame < _this.rangeEnd) {
                        _this.upDateSheet();
                        _this.currFrame++;
                    }
                    else {
                        _this.currState = "s_pause";
                    }
                }
                else {
                    // Playing backward
                    if (_this.currFrame > _this.rangeEnd) {
                        _this.upDateSheet();
                        _this.currFrame--;
                    }
                    else {
                        _this.currState = "s_pause";
                    }
                }
            }
            else if (_this.currState == "s_pause") {
                _this.ss_deActivate();
            }
        };
        this.upDateSheet = function () {
            var xpos = ((_this.currFrame - 1) % _this.spritesInX);
            var ypos = Math.floor(((_this.currFrame - 1) / _this.spritesInX));
            _this.animDiv.style.backgroundPosition = (-xpos * _this.frameWidth) + "px " + (-ypos * _this.frameHeight) + "px";
        };
        this.ss_activate = function () {
            _this.ss_deActivate();
            _this.mainLoop = setInterval(_this.requestFrame, _this.speed);
        };
        this.ss_deActivate = function () {
            clearInterval(this.mainLoop);
        };
        this.ss_play = function () {
            this.ss_activate();
            this.currState = "s_play";
        };
        this.ss_rewind = function () {
            _this.ss_activate();
            _this.currState = "s_rewind";
        };
        this.ss_pause = function () {
            this.currState = "s_pause";
        };
        this.ss_goToFrame = function (frame) {
            this.ss_deActivate();
            this.currFrame = frame;
            this.upDateSheet();
        };
        this.ss_playRange = function (start, end) {
            this.ss_activate();
            this.rangeStart = start;
            this.rangeEnd = end;
            this.currFrame = this.rangeStart;
            this.currState = "play_range";
        };
        this.animDiv = document.getElementById(animDiv);
        this.imgUrl = imgUrl;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.sheetWidth = sheetWidth;
        this.sheetHeight = sheetHeight;
        this.totalFrames = totalFrames;
        this.allowLoop = allowLoop;
        this.speed = speed;
        this.spritesInX = this.sheetWidth / this.frameWidth;
        this.spritesInY = this.sheetHeight / this.frameHeight;
        this.setUpSheet();
    }
    return spriteSheetAnim;
}());
